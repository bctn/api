<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



// Kiểm tra đăng nhập
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('list', 'AuthController@index');
});

// check logout
Route::group([
    'prefix' => 'auth',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('me', 'AuthController@me');
    });
});

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'user',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->get('getPaginate', 'UserController@getPaginate');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
//    });
});

// Team
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'team',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('getList', 'TeamController@index');
        $router->get('getPaginate', 'TeamController@getPaginate');
        $router->post('create', 'TeamController@create');
        $router->get('show/{id}', 'TeamController@show');
        $router->post('update/{id}', 'TeamController@update');
        $router->delete('remove/{id}', 'TeamController@remove');
        $router->post('searchAll', 'TeamController@searchAll');
//    });
});

// Unit
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'unit',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('getPaginate', 'UnitController@getPaginate');
        $router->post('create', 'UnitController@create');
        $router->get('show/{id}', 'UnitController@show');
        $router->post('update/{id}', 'UnitController@update');
        $router->delete('remove/{id}', 'UnitController@remove');
        $router->post('searchAll', 'UnitController@searchAll');
//    });
});

// OPTIONS

Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'unit',
], function (Registrar $router) {
// Get options
    $router->get('list', 'UnitController@list');
    $router->get('getClassrooms/{id}', 'TeamController@getClassrooms');
});


// SchoolYear
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'schoolYear',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
//        $router->get('list', 'SchoolYearController@index');
        $router->get('getPaginate', 'SchoolYearController@getPaginate');
        $router->post('create', 'SchoolYearController@create');
        $router->get('show/{id}', 'SchoolYearController@show');
        $router->post('update/{id}', 'SchoolYearController@update');
        $router->delete('remove/{id}', 'SchoolYearController@remove');
        $router->post('searchAll', 'SchoolYearController@searchAll');
//    });
});

// SchoolYear
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'schoolYear',
], function (Registrar $router) {
    $router->get('list', 'SchoolYearController@index');
});

// Staff
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'staff',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'StaffController@index');
        $router->get('getPaginate', 'StaffController@getPaginate');
        $router->post('create', 'StaffController@create');
        $router->get('show/{id}', 'StaffController@show');
        $router->post('update/{id}', 'StaffController@update');
        $router->delete('remove/{id}', 'StaffController@remove');
        $router->post('searchAll', 'StaffController@searchAll');
        $router->post('upload', 'StaffController@upload');
//    });
});

// CLASSROOM STUDENT
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'classroomStudent',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('getPaginate', 'ClassroomStudentController@getPaginate');
        $router->post('create', 'ClassroomStudentController@create');
        $router->get('show/{id}', 'ClassroomStudentController@show');
        $router->post('update/{id}', 'ClassroomStudentController@update');
        $router->delete('remove/{id}', 'ClassroomStudentController@remove');
        $router->post('searchAll', 'ClassroomStudentController@searchAll');
//    });
});

// Attendance
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'attendance',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list', 'AttendanceController@index');
    $router->post('create', 'AttendanceController@create');
    $router->get('show/{id}', 'AttendanceController@show');
    $router->post('update/{id}', 'AttendanceController@update');
    $router->delete('remove/{id}', 'AttendanceController@remove');
    $router->post('searchAll', 'AttendanceController@searchAll');

    // Test
    $router->post('getListSearch', 'AttendanceController@getListSearch');
//    });
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'news',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'NewsController@index');
        $router->post('create', 'NewsController@create');
        $router->get('show/{id}', 'NewsController@show');
        $router->post('update/{id}', 'NewsController@update');
        $router->delete('remove/{id}', 'NewsController@remove');
        $router->post('searchAll', 'NewsController@searchAll');
        $router->post('upload', 'NewsController@upload');
//    });
});

// CONTACT
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'contact',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'ContactController@index');
        $router->delete('remove/{id}', 'ContactController@remove');
        $router->post('searchAll', 'ContactController@searchAll');
//    });
});

// Thống kê số liệu
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'statistical',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'StatisticalController@index');
        $router->post('searchOptions', 'StatisticalController@searchOptions');
//    });
});


//=====================================================================================
// Attendance OF CUSTOMERAPI
Route::group([
    'namespace' => 'Api\\CustomerApi',
    'prefix'    => 'attendanceOfCustomer',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list/{code_number_teacher}', 'AttendanceController@index');
    $router->post('create', 'AttendanceController@create');
    $router->get('show/{id}', 'AttendanceController@show');
    $router->post('update/{id}', 'AttendanceController@update');
    $router->delete('remove/{id}', 'AttendanceController@remove');
    $router->post('searchAll', 'AttendanceController@searchAll');

    // Test
    $router->post('getListSearch', 'AttendanceController@getListSearch');
//    });
});