<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_unit',
        'code_number_team',
        'name',
    ];

    protected $filter = [
        'id',
        'code_number_unit',
        'code_number_team',
        'name',
    ];

    public function units()
    {
        return $this->belongsTo(Unit::class, 'code_number_unit', 'code_number_unit');
    }
}
