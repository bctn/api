<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
    ];

    protected $filter = [
        'id',
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
    ];
}
