<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_team',
        'code_number_staff',
        'date_absent',
        'absent',
        'reason_for_absence'
    ];

    protected $filter = [
        'id',
        'code_number_team',
        'code_number_staff',
        'date_absent',
        'absent',
        'reason_for_absence'
    ];

    public function teams()
    {
        return $this->belongsTo(Team::class, 'code_number_team', 'code_number_team');
    }

    public function staffs()
    {
        return $this->belongsTo(Staff::class, 'code_number_staff', 'code_number_staff');
    }
}
