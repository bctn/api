<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_unit',
        'name',
    ];

    protected $filter = [
        'id',
        'code_number_unit',
        'name',
    ];

    public function classroom()
    {
        return $this->hasMany(Team::class, 'code_number_unit', 'code_number_unit');
    }
}
