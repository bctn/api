<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                                 => 'required',
            'content'                               => 'required',
            'category_id'                           => 'required',
            'thumbnails'                            => 'required',
            'source'                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                        => 'Bạn chưa nhập tiêu đề',
            'content.required'                      => 'Bạn chưa nhập nội dung',
            'category_id.required'                  => 'Bạn chưa nhập loại tin',
            'thumbnails.required'                   => 'Bạn chưa nhập hình ảnh',
            'source.required'                       => 'Bạn chưa nhập nguồn trích dẫn',
        ];
    }
}
