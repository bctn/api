<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_team'                                     => 'required',
            'code_number_staff'                                     => 'required',
            'date_absent'                                           => 'required',
            'absent'                                                => 'required',
//            'reason_for_absence'                                    => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'code_number_team.required'                             => 'Bạn chưa nhập mã đội, đồn, tổ',
            'code_number_staff.required'                            => 'Bạn chưa nhập mã cán bộ',
            'date_absent.required'                                  => 'Bạn chưa nhập ngày vắng mặt',
            'absent.required'                                       => 'Bạn chưa nhập có/không phép vắng mặt',
        ];
    }
}
