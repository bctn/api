<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_staff'                                 => 'required',
            'slug'                                              => 'nullable',
            'last_name'                                         => 'required',
            'first_name'                                        => 'required',
            'sex'                                               => 'required',
            'birthday'                                          => 'required',
            'thumbnails'                                        => 'required',
            'email'                                             => 'required',
            'email_verified_at'                                 => 'nullable',
            'identity_number'                                   => 'required',
            'phone'                                             => 'required',
            'mobile'                                            => 'required',
            'mobile_verified_at'                                => 'nullable',
            'place_of_birth'                                    => 'required',
            'home_town'                                         => 'required',
            'permanent_residence'                               => 'required',
            'current_home'                                      => 'required',
            'nation'                                            => 'required',
            'religion'                                          => 'required',
            'nationality'                                       => 'required',
            'level'                                             => 'required',
            'specialized'                                       => 'required',


            'position_staff'                                    => 'required',
            'type_staff'                                        => 'required',
            'clan'                                              => 'nullable',
            'date_clan'                                         => 'nullable',

            'code_number_unit'                                  => 'nullable',
            'code_number_team'                                  => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'code_number_staff.required'                            => 'Bạn chưa nhập mã cán bộ',
//            'slug.required'                                          => 'Bạn chưa nhập tài khoản',
            'last_name.required'                                     => 'Bạn chưa nhập họ sinh viên',
            'first_name.required'                                    => 'Bạn chưa nhập tên sinh viên',
            'sex.required'                                           => 'Bạn chưa nhập giới tính',
            'birthday.required'                                      => 'Bạn chưa nhập ngày sinh',
            'thumbnails.required'                                    => 'Bạn chưa nhập ảnh đại diện',
            'email.required'                                         => 'Bạn chưa nhập địa chỉ email',
//            'email_verified_at.required'                             => 'Bạn chưa nhập tài khoản',
            'identity_number.required'                               => 'Bạn chưa nhập CMND',
            'phone.required'                                         => 'Bạn chưa nhập điện thoại bàn',
            'mobile.required'                                        => 'Bạn chưa nhập điện thoại di động',
//            'mobile_verified_at.required'                            => 'Bạn chưa nhập tài khoản',
            'place_of_birth.required'                                => 'Bạn chưa nhập nơi sinh',
            'home_town.required'                                     => 'Bạn chưa nhập quê quán',
            'permanent_residence.required'                           => 'Bạn chưa nhập hộ khẩu thường trú',
            'current_home.required'                                  => 'Bạn chưa nhập nơi ở hiện tại',
            'nation.required'                                        => 'Bạn chưa nhập dân tộc',
            'religion.required'                                      => 'Bạn chưa nhập tôn giáo',
            'nationality.required'                                   => 'Bạn chưa nhập quốc tịch',
            'level.required'                                         => 'Bạn chưa nhập trình độ',
            'specialized.required'                                   => 'Bạn chưa nhập chuyên ngành',

            'position_staff.required'                                => 'Bạn chưa nhập chức vụ cán bộ',
            'type_staff.required'                                    => 'Bạn chưa nhập loại cán bộ',

            'code_number_unit.required'                              => 'Bạn chưa nhập phòng ba',
            'code_number_team.required'                              => 'Bạn chưa nhập đội, đồn, tổ',
        ];
    }
}
