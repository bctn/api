<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_unit'                                      => 'required',
            'code_number_team'                                      => 'required',
            'name'                                                  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_unit.required'                             => 'Bạn chưa nhập mã phòng ban',
            'code_number_team.required'                             => 'Bạn chưa nhập mã đội, đồn, tổ',
            'name.required'                                         => 'Bạn chưa nhập tên đội, đồn, tổ',
        ];
    }
}
