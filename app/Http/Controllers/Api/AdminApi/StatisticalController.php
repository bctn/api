<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Staff;
use Illuminate\Http\Request;

class StatisticalController extends AbstractApiController
{
    public function searchOptions(Request $request)
    {
        $start= $request->start_date;
        $end= $request->end_date;

        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',

                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',

                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',

                'level',
                'specialized',

                'position_staff',
                'type_staff',
                'clan',
                'date_clan',

                'code_number_unit',
                'code_number_team',
            ])
            ->with('teams')
            ->where('clan', '=', true)
            ->whereBetween('date_clan', [$start, $end])
//            ->whereHas('decisions', function($decisions) use($start, $end) {
//                $decisions->whereBetween('received_date', [$start, $end]);
//            })
            ->get();

        return $this->item($staff);
    }
}
