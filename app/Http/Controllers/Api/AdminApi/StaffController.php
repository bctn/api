<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\StaffCreateRequest;
use App\Staff;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends AbstractApiController
{
    public function index()
    {
        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
                'position_staff',
                'type_staff',
                'clan',
                'date_clan',
            ])
            ->get();

        return $this->item($staff);
    }

    public function getPaginate(Request $request)
    {
        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
                'position_staff',
                'type_staff',
                'clan',
                'date_clan',
            ])
            ->DataTablePaginate($request);

        return $this->item($staff);
    }

    public function create(StaffCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $payload = [];

        $payload['code_number_staff']                           = $validatedData['code_number_staff'];
        $payload['slug']                                        = $slugify->slugify($validatedData['code_number_staff']);
        $payload['last_name']                                   = $validatedData['last_name'];
        $payload['first_name']                                  = $validatedData['first_name'];
        $payload['sex']                                         = $validatedData['sex'];
        $payload['birthday']                                    = $validatedData['birthday'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['email']                                       = $validatedData['email'];
        $payload['email_verified_at']                           = Carbon::now();
        $payload['identity_number']                             = $validatedData['identity_number'];
        $payload['phone']                                       = $validatedData['phone'];
        $payload['mobile']                                      = $validatedData['mobile'];
        $payload['mobile_verified_at']                          = Carbon::now();
        $payload['place_of_birth']                              = $validatedData['place_of_birth'];
        $payload['home_town']                                   = $validatedData['home_town'];
        $payload['permanent_residence']                         = $validatedData['permanent_residence'];
        $payload['current_home']                                = $validatedData['current_home'];
        $payload['nation']                                      = $validatedData['nation'];
        $payload['religion']                                    = $validatedData['religion'];
        $payload['nationality']                                 = $validatedData['nationality'];
        $payload['level']                                       = $validatedData['level'];
        $payload['specialized']                                 = $validatedData['specialized'];

        $payload['position_staff']                              = $validatedData['position_staff'];
        $payload['type_staff']                                  = $validatedData['type_staff'];
        $payload['clan']                                        = $validatedData['clan'];
        $payload['date_clan']                                   = $validatedData['date_clan'];

        $payload['code_number_unit']                            = $validatedData['code_number_unit'];
        $payload['code_number_team']                            = $validatedData['code_number_team'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['code_number_staff'])) {
            $this->setMessage('Đã tồn tại mã cán bộ');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $staff = Staff::create($payload);
        DB::beginTransaction();

        try {
            $staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm cán bộ thành công!');
            $this->setStatusCode(200);
            $this->setData($staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Staff::findOrFail($id);
    }

    public function update(StaffCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $staff = Staff::query()->findOrFail($id);
        if (! $staff) {
            $this->setMessage('Không có cán bộ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $staff->code_number_staff                           = $validatedData['code_number_staff'];
                $staff->slug                                        = $slugify->slugify($validatedData['code_number_staff']);
                $staff->last_name                                   = $validatedData['last_name'];
                $staff->first_name                                  = $validatedData['first_name'];
                $staff->sex                                         = $validatedData['sex'];
                $staff->birthday                                    = $validatedData['birthday'];
                $staff->thumbnails                                  = $validatedData['thumbnails'];
                $staff->email                                       = $validatedData['email'];
                $staff->email_verified_at                           = $validatedData['email_verified_at'];
                $staff->identity_number                             = $validatedData['identity_number'];
                $staff->phone                                       = $validatedData['phone'];
                $staff->mobile                                      = $validatedData['mobile'];
                $staff->mobile_verified_at                          = $validatedData['mobile_verified_at'];
                $staff->place_of_birth                              = $validatedData['place_of_birth'];
                $staff->home_town                                   = $validatedData['home_town'];
                $staff->permanent_residence                         = $validatedData['permanent_residence'];
                $staff->current_home                                = $validatedData['current_home'];
                $staff->nation                                      = $validatedData['nation'];
                $staff->religion                                    = $validatedData['religion'];
                $staff->nationality                                 = $validatedData['nationality'];
                $staff->level                                       = $validatedData['level'];
                $staff->specialized                                 = $validatedData['specialized'];

                $staff->position_staff                              = $validatedData['position_staff'];
                $staff->type_staff                                  = $validatedData['type_staff'];
                $staff->clan                                        = $validatedData['clan'];
                $staff->date_clan                                   = $validatedData['date_clan'];

                $staff->code_number_unit                            = $validatedData['code_number_unit'];
                $staff->code_number_team                            = $validatedData['code_number_team'];

                $staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Staff::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_staff)
    {
        $staff = Staff::query()->get();
        foreach ($staff->pluck('code_number_staff') as $item) {
            if ($code_number_staff == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
                'position_staff',
                'type_staff',
                'clan',
                'date_clan',
            ])
            ->where('code_number_staff', 'LIKE', "%$search%")
            ->orWhere('last_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($staff);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/staff'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
