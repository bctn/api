<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Http\Controllers\AbstractApiController;

use App\Attendance;
use App\Http\Requests\AttendanceCreateRequest;
use App\Staff;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends AbstractApiController
{
    public function getListSearch(Request $request)
    {
        $searchOptionTeam = $request->keyOptionTeam;
        if($searchOptionTeam) {
            $staff = Staff::query()
                ->select([
                    'id',
                    'code_number_staff',
                    'slug',
                    'last_name',
                    'first_name',
                    'sex',
                    'birthday',
                    'thumbnails',

                    'email',
                    'email_verified_at',
                    'identity_number',
                    'phone',
                    'mobile',
                    'mobile_verified_at',

                    'place_of_birth',
                    'home_town',
                    'permanent_residence',
                    'current_home',
                    'nation',
                    'religion',
                    'nationality',

                    'level',
                    'specialized',

                    'position_staff',
                    'type_staff',
                    'clan',
                    'date_clan',

                    'code_number_unit',
                    'code_number_team',
                ])
                ->with('teams')
                ->where('code_number_team', '=', $searchOptionTeam)
                ->DataTablePaginate($request);

            return $this->item($staff);
        }
        else {
            $staff = Staff::query()
                ->select([
                    'id',
                    'code_number_staff',
                    'slug',
                    'last_name',
                    'first_name',
                    'sex',
                    'birthday',
                    'thumbnails',

                    'email',
                    'email_verified_at',
                    'identity_number',
                    'phone',
                    'mobile',
                    'mobile_verified_at',

                    'place_of_birth',
                    'home_town',
                    'permanent_residence',
                    'current_home',
                    'nation',
                    'religion',
                    'nationality',

                    'level',
                    'specialized',

                    'position_staff',
                    'type_staff',
                    'clan',
                    'date_clan',

                    'code_number_unit',
                    'code_number_team',
                ])
                ->with('teams')
                ->DataTablePaginate($request);

            return $this->item($staff);
        }
    }

    public function index(Request $request)
    {
        $attendance = Attendance::query()
            ->select([
                'id',
                'code_number_team',
                'code_number_staff',
                'date_absent',
                'absent',
                'reason_for_absence'
            ])
            ->with('teams', 'staffs')
            ->DataTablePaginate($request);

        return $this->item($attendance);
    }

    public function create(AttendanceCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['code_number_staff'] as $item) {
            $payload['code_number_team']                                = $validatedData['code_number_team'];
            $payload['code_number_staff']                               = $item['code_number_staff'];
            $payload['date_absent']                                     = $validatedData['date_absent'];
            $payload['absent']                                          = ! empty($item['absent']) ? $item['absent'] : 0;
            $payload['reason_for_absence']                              = ! empty($item['reason_for_absence']) ? $item['reason_for_absence'] : "";

            // Kiểm tra trùng tên danh mục
//            if (! $this->checkDuplicateName($payload['code_number_student'])) {
//                $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
//                $this->setStatusCode(400);
//                return $this->respond();
//            }

            $check_attendance = Attendance::query()
                ->where('code_number_staff', '=' ,$payload['code_number_staff'])
                ->where('date_absent', '=' ,$payload['date_absent'])
                ->first();

            if($check_attendance) {
                $this->setMessage('Cán bộ đã tồn tại điểm danh');
                $this->setStatusCode(400);
                return $this->respond();
            }

            // Tạo và lưu tài khoản
            $attendance = Attendance::create($payload);
            DB::beginTransaction();

            try {
                $attendance->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Điểm danh thành công!');
                $this->setStatusCode(200);
                $this->setData($attendance);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Attendance::findOrFail($id);
    }

    public function update(AttendanceCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $attendance = Attendance::query()->findOrFail($id);
        if (! $attendance) {
            $this->setMessage('Không có vắng mặt này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $attendance->code_number_team                                    = $validatedData['code_number_team'];
                $attendance->code_number_staff                                  = $validatedData['code_number_staff'];
                $attendance->date_absent                                         = $validatedData['date_absent'];
                $attendance->absent                                              = ! empty($validatedData['absent']) ? $validatedData['absent'] : 0;
                $attendance->reason_for_absence                                  = ! empty($request->reason_for_absence) ? $request->reason_for_absence : "";

                $attendance->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($attendance);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Attendance::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $attendance = Attendance::query()
            ->select([
                'id',
                'code_number_team',
                'code_number_staff',
                'date_absent',
                'absent',
                'reason_for_absence'
            ])
            ->with('teams', 'staffs')
            ->whereHas('teams', function($teams) use($search) {
                $teams->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('staffs', function($staffs) use($search) {
                $staffs->where('first_name', 'LIKE', "%$search%");
                $staffs->orWhere('last_name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_team', 'LIKE', "%$search%")
            ->orWhere('reason_for_absence', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($attendance);
    }
}
