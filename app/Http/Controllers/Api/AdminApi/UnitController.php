<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Unit;
use App\Http\Requests\UnitCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UnitController extends AbstractApiController
{
    public function list()
    {
        $unit = Unit::query()
            ->select([
                'id',
                'code_number_unit',
                'name'
            ])
            ->get();

        return $this->item($unit);
    }

    public function getPaginate(Request $request)
    {
        $unit = Unit::query()
            ->select([
                'id',
                'code_number_unit',
                'name',
            ])
            ->DataTablePaginate($request);

        return $this->item($unit);
    }

    public function create(UnitCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_unit']                            = $validatedData['code_number_unit'];
        $payload['name']                                        = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên phòng ban');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $unit = Unit::create($payload);
        DB::beginTransaction();

        try {
            $unit->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm phòng ban thành công!');
            $this->setStatusCode(200);
            $this->setData($unit);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Unit::findOrFail($id);
    }

    public function update(UnitCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $unit = Unit::query()->findOrFail($id);
        if (! $unit) {
            $this->setMessage('Không có phòng ban này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $unit->code_number_unit                             = $validatedData['code_number_unit'];
                $unit->name                                         = $validatedData['name'];

                $unit->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($unit);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Unit::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $unit = Unit::query()->get();
        foreach ($unit->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $unit = Unit::query()
            ->select([
                'id',
                'code_number_unit',
                'name',
            ])
            ->where('code_number_unit', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($unit);
    }
}
