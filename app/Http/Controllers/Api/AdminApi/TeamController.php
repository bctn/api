<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Team;
use App\Http\Requests\TeamCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends AbstractApiController
{
    public function index()
    {
        $team = Team::query()
            ->select([
                'id',
                'code_number_unit',
                'code_number_team',
                'name',
            ])
            ->with('units')
            ->get();

        return $this->item($team);
    }

    public function getClassrooms($id)
    {
        $team = Team::query()
            ->select([
                'id',
                'code_number_unit',
                'code_number_team',
                'name',
            ])
            ->with('units')
            ->where('code_number_unit', '=', $id)
            ->get();

        return $this->item($team);
    }

    public function getPaginate(Request $request)
    {
        $team = Team::query()
            ->select([
                'id',
                'code_number_unit',
                'code_number_team',
                'name',
            ])
            ->with('units')
            ->DataTablePaginate($request);

        return $this->item($team);
    }

    public function create(TeamCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_unit']                            = $validatedData['code_number_unit'];
        $payload['code_number_team']                            = $validatedData['code_number_team'];
        $payload['name']                                        = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['code_number_team'])) {
            $this->setMessage('Đã tồn tại mã đội, đồn, tổ');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $team = Team::create($payload);
        DB::beginTransaction();

        try {
            $team->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm đội, đồn, tổ thành công!');
            $this->setStatusCode(200);
            $this->setData($team);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Team::findOrFail($id);
    }

    public function update(TeamCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $team = Team::query()->findOrFail($id);
        if (! $team) {
            $this->setMessage('Không có đội, đồn, tổ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $team->code_number_unit                         = $validatedData['code_number_unit'];
                $team->code_number_team                         = $validatedData['code_number_team'];
                $team->name                                     = $validatedData['name'];

                $team->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($team);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Team::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_team)
    {
        $team = Team::query()->get();
        foreach ($team->pluck('code_number_team') as $item) {
            if ($code_number_team == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $team = Team::query()
            ->select([
                'id',
                'code_number_unit',
                'code_number_team',
                'name',
            ])
            ->with('units')
            ->whereHas('units', function($units) use($search) {
                $units->where('name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_unit', 'LIKE', "%$search%")
            ->orWhere('code_number_team', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($team);
    }
}
