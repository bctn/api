<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\NewsCreateRequest;
use App\News;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends AbstractApiController
{
    public function index(Request $request)
    {
        $news = News::query()
            ->select([
                'id',
                'title',
                'content',
                'category_id',
                'thumbnails',
                'source',
            ])
            ->DataTablePaginate($request);

        return $this->item($news);
    }

    public function create(NewsCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['title']                           = $validatedData['title'];
        $payload['slug']                            = $slugify->slugify($validatedData['title']);
        $payload['content']                         = $validatedData['content'];
        $payload['category_id']                     = $validatedData['category_id'];
        $payload['thumbnails']                      = $validatedData['thumbnails'];
        $payload['source']                          = $validatedData['source'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $news = News::create($payload);
        DB::beginTransaction();

        try {
            $news->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tin tức thành công!');
            $this->setStatusCode(200);
            $this->setData($news);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return News::query()->findOrFail($id);
    }

    public function update(NewsCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $news = news::query()->findOrFail($id);
        if (!$news) {
            $this->setMessage('Không có tin tức này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $news->title                            = $validatedData['title'];
                $news->slug                             = $slugify->slugify($validatedData['title']);
                $news->content                          = $validatedData['content'];
                $news->category_id                      = $validatedData['category_id'];
                $news->thumbnails                       = $validatedData['thumbnails'];
                $news->source                           = $validatedData['source'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                $news->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($news);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        News::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $news = News::query()->get();
        foreach ($news->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $news = News::query()
            ->select([
                'id',
                'title',
                'content',
                'category_id',
                'thumbnails',
                'source',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->orWhere('content', 'LIKE', "%$search%")
            ->orWhere('source', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($news);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/news'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
