<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;
use Illuminate\Support\Facades\DB;

class UserController extends AbstractApiController
{
    public function index()
    {
        return response()->json(User::with('staffs')->get(), 200);
    }

    public function getPaginate(Request $request)
    {
        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'code_number_staff',
                'role',
                'status'
            ])
            ->DataTablePaginate($request);

        return $this->item($User);
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['username']                                = $validatedData['username'];
        $payload['password']                                = bcrypt($validatedData['password']);
        $payload['code_number_staff']                       = $validatedData['code_number_staff'];
        $payload['role']                                    = $validatedData['role'];
        $payload['status']                                  = true;

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tên tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $User = User::create($payload);
        DB::beginTransaction();

        try {
            $User->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($User);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tài khoản
                $User->username                                 = $validatedData['username'];
                $User->password                                 = bcrypt($validatedData['password']);
                $User->code_number_staff                        = $validatedData['code_number_staff'];
                $User->role                                     = $validatedData['role'];
                $User->status                                   = true;

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        User::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($username)
    {
        $User = User::query()->get();
        foreach ($User->pluck('username') as $item) {
            if ($username == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'code_number_staff',
                'role',
                'status'
            ])
            ->where('username', 'LIKE', "%$search%")
            ->orWhere('code_number_staff', 'LIKE', "%$search%")
            ->orWhere('role', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($User);
    }
}
