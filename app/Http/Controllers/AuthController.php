<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthCreateRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client;
use Illuminate\Http\Request;

class AuthController extends AbstractApiController
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = new User();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);

        if ($user->save()) {
            return response()->json([
                'message' => 'User tạo thành công!'
            ], 200);
        } else {
            return response()->json([
                'message' => 'User thất bại!'
            ], 500);
        }
    }

    // public function login(AuthCreateRequest $request) {

    //     $passwordGrantClient = Client::where('password_client', 1)->first();

    //     $data = [
    //         'grant_type' => 'password',
    //         'client_id' => $passwordGrantClient->id,
    //         'client_secret' => $passwordGrantClient->secret,
    //         'username' => $request->username,
    //         'password' => $request->password,
    //         'scope' => '*',
    //     ];

    //     $tokenRequest = Request::create('/oauth/token', 'post', $data);

    //     $tokenResponse = app()->handle($tokenRequest);
    //     $contentString = $tokenResponse->content();
    //     $tokenContent = json_decode($contentString, true);

    //     if(!empty($tokenContent['access_token'])) {
    //         return $tokenResponse;
    //     }

    //     return response()->json([
    //         'message' => 'Unauthenticated',
    //     ]);
    // }

    public function login(Request $request)
   {
        $loginData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
       
        if(!auth()->attempt($loginData)) {
            return response(['message'=>'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);

   }

    public function logout(Request $request) {

        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Logout thành công',
            'status_code' => 200
        ]);
    }

    public function me() {
        return Auth::user();
    }
}
