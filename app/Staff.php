<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use DataTablePaginate;

    protected $table = "staffs";

    protected $fillable = [
        'code_number_staff',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',

        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',

        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',

        'level',
        'specialized',

        'position_staff',
        'type_staff',
        'clan',
        'date_clan',

        'code_number_unit',
        'code_number_team',
    ];

    protected $filter = [
        'id',
        'code_number_staff',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',

        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',

        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',

        'level',
        'specialized',

        'position_staff',
        'type_staff',
        'clan',
        'date_clan',

        'code_number_unit',
        'code_number_team',
    ];

    public function teams()
    {
        return $this->belongsTo(Team::class, 'code_number_team', 'code_number_team');
    }
}
