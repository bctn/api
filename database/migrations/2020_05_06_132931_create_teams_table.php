<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_team', 200)->unique()->comment('Mã đội, đồn, tổ');
            $table->string('name', 200)->comment('Tên đội, đồn, tổ');

            $table->string('code_number_unit', 200)->comment('Mã phòng ban');

            $table->index(['code_number_unit', 'code_number_team']);

            $table->foreign('code_number_unit')
                ->references('code_number_unit')->on('units')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
