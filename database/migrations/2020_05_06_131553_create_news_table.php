<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiên, SEO URL');
            $table->string('title', 200)->comment('Tiêu đề tin tức');
            $table->longText('content')->comment('Nôi dung');
            $table->bigInteger('category_id')->comment('ID danh mục tin tức');

            // Hình ảnh
            $table->string('thumbnails')->default('')->comment('Ảnh đại diên');

            // Thông tin khác
            $table->string('source', 200)->comment('Nguồn tin');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
