<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('username');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');;

            $table->string('code_number_staff')->nullable()->comment('Cán bộ');

            $table->string('role')->default('customer')->comment('Quyền hạn (admin, staff)');
            $table->boolean('status')->default(true)->comment('Tình trạng khóa tài khoản');

            $table->foreign('code_number_staff')
                ->references('code_number_staff')->on('staffs')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            // Tạo code để reset password
            $table->string('code')->nullable()->index();
            $table->timestamp('time_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
