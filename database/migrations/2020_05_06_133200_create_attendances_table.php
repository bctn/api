<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_team')->default('')->comment('Đội, đồn, tổ');
            $table->string('code_number_staff')->default('')->comment('Cán bộ');

            $table->date('date_absent')->comment('Ngày vắng mặt');
            $table->unsignedTinyInteger('absent')->default(1)->comment('0: Không vắng, 1: Vắng có phép, 2: Vắng không phép');
            $table->string('reason_for_absence')->nullable()->comment('Lý do vắng mặt');

            $table->foreign('code_number_staff')
                ->references('code_number_staff')->on('staffs')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
