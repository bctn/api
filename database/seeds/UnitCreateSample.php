<?php

use App\Unit;
use Illuminate\Database\Seeder;

class UnitCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/unit.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Unit ${idx}/${c}";

            if (empty($item['code_number_unit'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $unit = Unit::create($item);
            $unit->save();
        }

        echo PHP_EOL;
    }
}
