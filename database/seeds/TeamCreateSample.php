<?php

use App\Team;
use Illuminate\Database\Seeder;

class TeamCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/team.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Team ${idx}/${c}";

            if (empty($item['code_number_team'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $team = Team::create($item);
            $team->save();
        }

        echo PHP_EOL;
    }
}
