<?php

use Illuminate\Database\Seeder;
use App\Staff;

class StaffCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/staff.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Staff ${idx}/${c}";

            if (empty($item['code_number_staff'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $staff = Staff::create($item);
            $staff->save();
        }

        echo PHP_EOL;
    }
}
