<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            // Thông tin đăng nhập
            'username'              => 'demoad',
            'password'              => bcrypt('123456'),
            'code_number_staff'     => 'NV001',

            // Quyền hạn
            'role'                  => 'admin',
            'status'                => true,

        ]);
    }
}
